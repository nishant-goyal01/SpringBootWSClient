package com.springboot.client.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.springboot.client.SpringBootClientApplication;
import com.springboot.client.executors.PayloadDataExecutor;
import com.springboot.client.payload.PayloadData;

@Controller
public class PayloadDataController {


	@Autowired
	private RestTemplate restTemplateInst;

	@Autowired
	private ExecutorService exServiceInst;


	@RequestMapping("/payloadData/{seqno}")
	@ResponseBody
	public String getPayloadData(@PathVariable String seqno){

		CompletableFuture<PayloadData> payloadDataFuture = CompletableFuture.supplyAsync(()->{
			HttpHeaders requestHeaders=new HttpHeaders();
			List<MediaType> acceptableMediaTypes=new ArrayList<MediaType>();
			acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
			try{
				requestHeaders.setAccept(acceptableMediaTypes);
				HttpEntity<?> requestEntity=new HttpEntity<Object>(requestHeaders);

				HttpEntity<PayloadData> httpEntity = restTemplateInst.exchange(SpringBootClientApplication.url, HttpMethod.GET, requestEntity, PayloadData.class, seqno);
				return httpEntity.getBody();
			}
			catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}, exServiceInst);

		CompletableFuture<Object> futureData = payloadDataFuture.handle((ok, ex) -> {
			if (ok != null) {
				return ok.toString();
			} else {
				System.out.println(ex.getMessage());
				return "Got Error while calling the WS";
			}
		});
		try {
			return futureData.get().toString();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Got Error while fetching the result";
		}
		
	}


}
