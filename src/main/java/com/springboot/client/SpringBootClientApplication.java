package com.springboot.client;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.springboot.client.executors.PayloadDataExecutor;
import com.springboot.client.payload.PayloadData;

import scala.annotation.meta.getter;

@Configuration
@ComponentScan
@EnableAutoConfiguration 
// @enable
public class SpringBootClientApplication {

	public static final String url="http://192.168.102.37:8081" + "/jsonResp/{sequence}";
	RestTemplate template;
	ExecutorService exService;
	public static void main(String[] args) {
		/*SpringApplication app = new SpringApplication(SpringBootClientApplication.class);
		app.setShowBanner(false);
		ApplicationContext appCtxt = app.run( args);*/
		ApplicationContext appCtxt = SpringApplication.run(SpringBootClientApplication.class,args);
		PayloadDataExecutor executor = appCtxt.getBean(PayloadDataExecutor.class);
		CompletableFuture<PayloadData>[] payloadDataFuture = new CompletableFuture[10];
		for(int i=0;i<10;i++){
			System.out.println("In Application CALLING executor with seqNo "+i);
			payloadDataFuture[i] = executor.callPayloadDataService(i+"");
		}
		for(int i=0;i<10;i++){
			try {
				System.out.println("In Application Result "+payloadDataFuture[i].get().toString());
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	@Bean
	public RestTemplate restTemplateInst(){

		template = new RestTemplate();
		return template;
	}
	@Bean
	public ExecutorService exServiceInst(){
		exService = Executors.newCachedThreadPool();
		return exService;
	}
}
