package com.springboot.client.executors;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.springboot.client.SpringBootClientApplication;
import com.springboot.client.payload.PayloadData;

@Service
public class PayloadDataExecutor {

	@Autowired
	private RestTemplate restTemplateInst;

	@Autowired
	private ExecutorService exServiceInst;

	public CompletableFuture<PayloadData> callPayloadDataService(String seqno){

		CompletableFuture<PayloadData> payloadDataFuture = CompletableFuture.supplyAsync(()->{
			System.out.println("In Executor seqno " + seqno +" Thread "+Thread.currentThread().getName());
			HttpHeaders requestHeaders=new HttpHeaders();
			List<MediaType> acceptableMediaTypes=new ArrayList<MediaType>();
			acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
			try{
				requestHeaders.setAccept(acceptableMediaTypes);
				HttpEntity<?> requestEntity=new HttpEntity<Object>(requestHeaders);
				//Thread.currentThread().wait(1000);
				//Thread.sleep(100);
				HttpEntity<PayloadData> httpEntity = restTemplateInst.exchange(SpringBootClientApplication.url, HttpMethod.GET, requestEntity, PayloadData.class, seqno);
				System.out.println("In Executor seqno " + seqno +" Thread "+Thread.currentThread().getName() +" Res From Client "+ httpEntity.getBody());
				return httpEntity.getBody();
				
			}
			catch(Exception e){
				e.printStackTrace();
				return null;
			}
			
		}, exServiceInst);
		return payloadDataFuture;
	}

}
